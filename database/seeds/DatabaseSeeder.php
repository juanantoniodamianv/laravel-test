<?php

use Illuminate\Database\Seeder;
use App\Model\Persona;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        Persona::create([
            "name"=>"Tonio",
            "surname"=>"Juan",
            "age"=>29
        ]);
    }
}
