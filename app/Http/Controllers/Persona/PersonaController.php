<?php

namespace App\Http\Controllers\Persona;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Persona;

class PersonaController extends Controller
{
    function addPersona(Request $request, Persona $unaPersona) 
    {
        $unaPersona->name = $request->name;
        $unaPersona->surname = $request->surname;
        $unaPersona->age = $request->age;
        $unaPersona->save();
        return redirect()->route('persona');
    }
}
