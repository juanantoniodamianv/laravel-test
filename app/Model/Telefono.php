<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Telefono extends Model
{
    protected $table = 'Telefonos';
    public $timestamps = false;
}
