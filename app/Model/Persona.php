<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = 'Personas';
    public $timestamps = false;

    public function telefonos()
    {
        return $this->Relation('App\Model\Telefono');
    } 
}
